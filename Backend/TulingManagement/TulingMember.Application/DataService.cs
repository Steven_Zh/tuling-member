﻿using Furion;
using Furion.DatabaseAccessor;
using Furion.DynamicApiController;
using Furion.FriendlyException;
using Furion.UnifyResult;
using Mapster;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TulingMember.Application.Dto;
using TulingMember.Core; 
using Yitter.IdGenerator;

namespace TulingMember.Application
{
    public class DataService : IDynamicApiController
    {
        private readonly ILogger _logger;
      
        private readonly IRepository<cts_Customer> _customerRepository;
        private readonly IRepository<cts_SaleOrder> _saleorderRepository;
        private readonly IRepository<cts_CustomerPay> _customerpayRepository; 
        public DataService(ILogger logger
       
            , IRepository<cts_Customer> customerRepository
            , IRepository<cts_SaleOrder> saleorderRepository
            , IRepository<cts_CustomerPay> customerpayRepository)
        {
            _logger = logger;
          
            _customerRepository = customerRepository;
            _saleorderRepository = saleorderRepository;
            _customerpayRepository = customerpayRepository;
        }


        
        #region 报表
        /// <summary>
        /// 首页数据
        /// </summary>
        public dynamic GetHomeData(){

            var customerCount = _customerRepository.Count();
            var customerBalance=_customerRepository.AsQueryable().Sum(m => m.Balance);
            var startDate = DateTime.Now.AddDays(1 - DateTime.Now.Day).Date;
            var endDate = DateTime.Now.Date.AddDays(1);
            var nextMonth = DateTime.Now.AddDays(1 - DateTime.Now.Day).AddMonths(1).Date;
            
            //销售
            var rstSale= _saleorderRepository.Where(m => m.OrderDate >= startDate && m.OrderDate < endDate).GroupBy(m => m.OrderDate)
               .Select(m => new 
               {
                   OrderDate = m.Key,
                   OrderCount=m.Count(),
                   AllFee = m.Sum(m=>m.AllFee)
               }).ToList();
            

            List<SaleAmountByDateDto> listSaleAmount=new List<SaleAmountByDateDto>(); 
            var currentDate = startDate;
            while (currentDate<endDate)
            {
                SaleAmountByDateDto item = new SaleAmountByDateDto
                {
                    OrderDate = currentDate.ToString("yyyy-MM-dd")
                };
                item.AllFee= rstSale.Where(m=>m.OrderDate==currentDate).Select(m=>m.AllFee).FirstOrDefault();
                 item.OrderCount= rstSale.Where(m => m.OrderDate == currentDate).Select(m => m.OrderCount).FirstOrDefault();
                listSaleAmount.Add(item); 
                currentDate = currentDate.AddDays(1);
            }

            return new {
                CustomerCount=customerCount,
                CustomerBalance=customerBalance,
                SaleOrderFee = rstSale.Sum(m=>m.AllFee),
                SaleOrderCount = rstSale.Sum(m => m.OrderCount),
                listSaleAmount = listSaleAmount,
              
            };

          }

        #endregion
    }
}
