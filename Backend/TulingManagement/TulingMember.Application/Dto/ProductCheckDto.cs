﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TulingMember.Application 
{
    public class ProductCheckDto
    {
        public long Id { get; set; }
        public decimal CheckNum { get; set; }
    }
}
