﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TulingMember.Application
{
    public class BaseInput : PageDto
    {
        public string keyword { get; set; }
        /// <summary>
        /// 开始日期
        /// </summary>
        public DateTime? sdate { get; set; }
        /// <summary>
        /// 结束日期
        /// </summary>
        public DateTime? edate { get; set; }
        public int status { get; set; }
        public int type { get; set; }
        public long customerid { get; set; }
        public long supplierid { get; set; }
        public long materialid { get; set; }
        public long productid { get; set; }
        public long typeid { get; set; }
    }
}
