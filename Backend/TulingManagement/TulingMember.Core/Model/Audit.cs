﻿using Furion.DatabaseAccessor;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TulingMember.Core
{
    public class Audit 
    {
        /// <summary>
        /// 表名
        /// </summary> 
        public string Table { get; set; }
        
        /// <summary>
        /// 列名
        /// </summary> 
        public string Column   { get; set; }

        /// <summary>
        /// 新值
        /// </summary> 
        public object NewValue { get; set; }

        /// <summary>
        /// 旧值
        /// </summary>    
        public object OldValue { get; set; }

        /// <summary>
        /// 操作
        /// </summary> 
        public string Operate { get; set; }
         /// <summary>
        /// 创建时间
        /// </summary> 
        public DateTime CreatedTime { get; set; } = DateTime.Now;
        /// <summary>
        /// 创建人Id
        /// </summary> 
        public long CreatedUserId { get; set; }
        /// <summary>
        /// 创建人姓名
        /// </summary>
        [StringLength(50)]
        public string CreatedUser { get; set; }
        

        /// <summary>
        /// 批次号
        /// </summary> 
        public long BatchNo { get; set; }
    }
}
